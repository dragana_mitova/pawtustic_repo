namespace todo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AnimalAction
    {
        public long AnimalActionID { get; set; }

        public Guid AnimalID { get; set; }

        public int ActionType { get; set; }

        public Guid UserID { get; set; }

        public string ActionValue { get; set; }

        public bool IsDeleted { get; set; }

        public virtual Animal Animal { get; set; }

        public virtual AnimalUser AnimalUser { get; set; }
    }
}
