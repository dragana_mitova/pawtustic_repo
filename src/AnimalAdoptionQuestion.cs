namespace todo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AnimalAdoptionQuestion
    {
        [Key]
        public long QuestionID { get; set; }

        public Guid AnimalID { get; set; }

        [Required]
        public string QuestionValue { get; set; }

        public virtual Animal Animal { get; set; }
    }
}
