namespace todo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AnimalCharacteristic
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AnimalCharacteristic()
        {
            AnimalCharValues = new HashSet<AnimalCharValue>();
        }

        [Key]
        public long CharacteristicID { get; set; }

        [Required]
        [StringLength(350)]
        public string CharacteristicValue { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreatedOn { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AnimalCharValue> AnimalCharValues { get; set; }
    }
}
