namespace todo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AnimalUser")]
    public partial class AnimalUser
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AnimalUser()
        {
            Animals = new HashSet<Animal>();
            Animals1 = new HashSet<Animal>();
            AnimalActions = new HashSet<AnimalAction>();
            AnimalEmails = new HashSet<AnimalEmail>();
            AnimalEmails1 = new HashSet<AnimalEmail>();
            AnimalNotifications = new HashSet<AnimalNotification>();
        }

        [Key]
        public Guid UserID { get; set; }

        [Required]
        [StringLength(250)]
        public string Name { get; set; }

        [Required]
        [StringLength(250)]
        public string Location { get; set; }

        [Required]
        [StringLength(150)]
        public string Country { get; set; }

        [Required]
        [StringLength(150)]
        public string City { get; set; }

        [StringLength(150)]
        public string Latitude { get; set; }

        [StringLength(150)]
        public string Longitude { get; set; }

        [Required]
        [StringLength(350)]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        public bool IsOrganization { get; set; }

        [Required]
        public string ImageURL { get; set; }

        public int Sex { get; set; }

        [StringLength(150)]
        public string MobileNumber { get; set; }

        [StringLength(50)]
        public string Rating { get; set; }

        public bool IsMobileVisible { get; set; }

        public DateTime? DateBorn { get; set; }

        public bool IsVip { get; set; }

        public bool IsDeleted { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Animal> Animals { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Animal> Animals1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AnimalAction> AnimalActions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AnimalEmail> AnimalEmails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AnimalEmail> AnimalEmails1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AnimalNotification> AnimalNotifications { get; set; }
    }
}
