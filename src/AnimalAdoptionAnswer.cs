namespace todo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AnimalAdoptionAnswer
    {
        [Key]
        public long AnswerID { get; set; }

        public Guid AnimalID { get; set; }

        [Required]
        public string AnswerValue { get; set; }

        public virtual Animal Animal { get; set; }
    }
}
