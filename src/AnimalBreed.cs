namespace todo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AnimalBreed")]
    public partial class AnimalBreed
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AnimalBreed()
        {
            Animals = new HashSet<Animal>();
        }

        [Key]
        public long BreedID { get; set; }

        [Required]
        [StringLength(150)]
        public string BreedName { get; set; }

        [StringLength(250)]
        public string BreedInfo { get; set; }

        public int BreedType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Animal> Animals { get; set; }
    }
}
