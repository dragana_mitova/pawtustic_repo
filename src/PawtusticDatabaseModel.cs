namespace todo
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class PawtusticDatabaseModel : DbContext
    {
        public PawtusticDatabaseModel()
            : base("name=PawtusticDatabaseModel")
        {
        }

        public virtual DbSet<Animal> Animals { get; set; }
        public virtual DbSet<AnimalAction> AnimalActions { get; set; }
        public virtual DbSet<AnimalAdoptionAnswer> AnimalAdoptionAnswers { get; set; }
        public virtual DbSet<AnimalAdoptionQuestion> AnimalAdoptionQuestions { get; set; }
        public virtual DbSet<AnimalBreed> AnimalBreeds { get; set; }
        public virtual DbSet<AnimalCharacteristic> AnimalCharacteristics { get; set; }
        public virtual DbSet<AnimalCharValue> AnimalCharValues { get; set; }
        public virtual DbSet<AnimalEmail> AnimalEmails { get; set; }
        public virtual DbSet<AnimalImage> AnimalImages { get; set; }
        public virtual DbSet<AnimalNotification> AnimalNotifications { get; set; }
        public virtual DbSet<AnimalUser> AnimalUsers { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Animal>()
                .HasMany(e => e.Animal1)
                .WithOptional(e => e.Animal2)
                .HasForeignKey(e => e.GroupAnimalID);

            modelBuilder.Entity<Animal>()
                .HasMany(e => e.AnimalActions)
                .WithRequired(e => e.Animal)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Animal>()
                .HasMany(e => e.AnimalAdoptionAnswers)
                .WithRequired(e => e.Animal)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Animal>()
                .HasMany(e => e.AnimalAdoptionQuestions)
                .WithRequired(e => e.Animal)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Animal>()
                .HasMany(e => e.AnimalCharValues)
                .WithRequired(e => e.Animal)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Animal>()
                .HasMany(e => e.AnimalImages)
                .WithRequired(e => e.Animal)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Animal>()
                .HasMany(e => e.AnimalNotifications)
                .WithRequired(e => e.Animal)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AnimalCharacteristic>()
                .HasMany(e => e.AnimalCharValues)
                .WithRequired(e => e.AnimalCharacteristic)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AnimalUser>()
                .HasMany(e => e.Animals)
                .WithRequired(e => e.AnimalUser)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AnimalUser>()
                .HasMany(e => e.Animals1)
                .WithOptional(e => e.AnimalUser1)
                .HasForeignKey(e => e.AdoptedByUserID);

            modelBuilder.Entity<AnimalUser>()
                .HasMany(e => e.AnimalActions)
                .WithRequired(e => e.AnimalUser)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AnimalUser>()
                .HasMany(e => e.AnimalEmails)
                .WithRequired(e => e.AnimalUser)
                .HasForeignKey(e => e.MailFrom)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AnimalUser>()
                .HasMany(e => e.AnimalEmails1)
                .WithRequired(e => e.AnimalUser1)
                .HasForeignKey(e => e.MailTo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AnimalUser>()
                .HasMany(e => e.AnimalNotifications)
                .WithRequired(e => e.AnimalUser)
                .WillCascadeOnDelete(false);
        }
    }
}
