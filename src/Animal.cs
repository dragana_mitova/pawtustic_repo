namespace todo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Animal")]
    public partial class Animal
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Animal()
        {
            Animal1 = new HashSet<Animal>();
            AnimalActions = new HashSet<AnimalAction>();
            AnimalAdoptionAnswers = new HashSet<AnimalAdoptionAnswer>();
            AnimalAdoptionQuestions = new HashSet<AnimalAdoptionQuestion>();
            AnimalCharValues = new HashSet<AnimalCharValue>();
            AnimalImages = new HashSet<AnimalImage>();
            AnimalNotifications = new HashSet<AnimalNotification>();
        }

        public Guid AnimalID { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        public int AnimalType { get; set; }

        public long? BreedID { get; set; }

        public int Sex { get; set; }

        public int? Age { get; set; }

        public int? Weight { get; set; }

        [Required]
        [StringLength(250)]
        public string Location { get; set; }

        [Required]
        [StringLength(150)]
        public string Country { get; set; }

        [Required]
        [StringLength(150)]
        public string City { get; set; }

        [StringLength(150)]
        public string Latitude { get; set; }

        [StringLength(150)]
        public string Longitude { get; set; }

        public int Status { get; set; }

        [StringLength(50)]
        public string TransportFee { get; set; }

        public int TransportType { get; set; }

        public Guid? GroupAnimalID { get; set; }

        public int Size { get; set; }

        public int? HairType { get; set; }

        public long Views { get; set; }

        public bool IsDeleted { get; set; }

        public Guid? AdoptedByUserID { get; set; }

        public Guid CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime? UpdatedOn { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Animal> Animal1 { get; set; }

        public virtual Animal Animal2 { get; set; }

        public virtual AnimalBreed AnimalBreed { get; set; }

        public virtual AnimalUser AnimalUser { get; set; }

        public virtual AnimalUser AnimalUser1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AnimalAction> AnimalActions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AnimalAdoptionAnswer> AnimalAdoptionAnswers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AnimalAdoptionQuestion> AnimalAdoptionQuestions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AnimalCharValue> AnimalCharValues { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AnimalImage> AnimalImages { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AnimalNotification> AnimalNotifications { get; set; }
    }
}
