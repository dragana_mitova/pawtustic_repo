namespace todo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AnimalCharValue
    {
        [Key]
        public long AnimalCharID { get; set; }

        public Guid AnimalID { get; set; }

        public long CharacteristicID { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreatedOn { get; set; }

        public Guid CreatedBy { get; set; }

        public virtual Animal Animal { get; set; }

        public virtual AnimalCharacteristic AnimalCharacteristic { get; set; }
    }
}
