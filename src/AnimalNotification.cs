namespace todo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AnimalNotification")]
    public partial class AnimalNotification
    {
        [Key]
        public long NotificationID { get; set; }

        public Guid AnimalID { get; set; }

        public Guid UserID { get; set; }

        public int NotificationType { get; set; }

        public string NotificationText { get; set; }

        [StringLength(150)]
        public string ApproveStatus { get; set; }

        public virtual Animal Animal { get; set; }

        public virtual AnimalUser AnimalUser { get; set; }
    }
}
