namespace todo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AnimalEmail")]
    public partial class AnimalEmail
    {
        [Key]
        public long EmailID { get; set; }

        public Guid MailFrom { get; set; }

        public Guid MailTo { get; set; }

        [Required]
        public string MailText { get; set; }

        public DateTime CreatedOn { get; set; }

        public int MailStatus { get; set; }

        public virtual AnimalUser AnimalUser { get; set; }

        public virtual AnimalUser AnimalUser1 { get; set; }
    }
}
