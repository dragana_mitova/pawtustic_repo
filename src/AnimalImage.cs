namespace todo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AnimalImage
    {
        public long AnimalImageId { get; set; }

        public Guid AnimalID { get; set; }

        [Required]
        public string ImageURL { get; set; }

        public bool IsDeleted { get; set; }

        public virtual Animal Animal { get; set; }
    }
}
