﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace todo
{
    public class AnimalsController : Controller
    {
        private PawtusticDatabaseModel db = new PawtusticDatabaseModel();

        // GET: Animals
        public async Task<ActionResult> Index()
        {
            var animals = db.Animals.Include(a => a.Animal2).Include(a => a.AnimalBreed).Include(a => a.AnimalUser).Include(a => a.AnimalUser1);
            return View(await animals.ToListAsync());
        }

        // GET: Animals/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Animal animal = await db.Animals.FindAsync(id);
            if (animal == null)
            {
                return HttpNotFound();
            }
            return View(animal);
        }

        // GET: Animals/Create
        public ActionResult Create()
        {
            ViewBag.GroupAnimalID = new SelectList(db.Animals, "AnimalID", "Name");
            ViewBag.BreedID = new SelectList(db.AnimalBreeds, "BreedID", "BreedName");
            ViewBag.CreatedBy = new SelectList(db.AnimalUsers, "UserID", "Name");
            ViewBag.AdoptedByUserID = new SelectList(db.AnimalUsers, "UserID", "Name");
            return View();
        }

        // POST: Animals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "AnimalID,Name,AnimalType,BreedID,Sex,Age,Weight,Location,Country,City,Latitude,Longitude,Status,TransportFee,TransportType,GroupAnimalID,Size,HairType,Views,IsDeleted,AdoptedByUserID,CreatedBy,CreatedOn,UpdatedOn")] Animal animal)
        {
            if (ModelState.IsValid)
            {
                animal.AnimalID = Guid.NewGuid();
                db.Animals.Add(animal);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.GroupAnimalID = new SelectList(db.Animals, "AnimalID", "Name", animal.GroupAnimalID);
            ViewBag.BreedID = new SelectList(db.AnimalBreeds, "BreedID", "BreedName", animal.BreedID);
            ViewBag.CreatedBy = new SelectList(db.AnimalUsers, "UserID", "Name", animal.CreatedBy);
            ViewBag.AdoptedByUserID = new SelectList(db.AnimalUsers, "UserID", "Name", animal.AdoptedByUserID);
            return View(animal);
        }

        // GET: Animals/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Animal animal = await db.Animals.FindAsync(id);
            if (animal == null)
            {
                return HttpNotFound();
            }
            ViewBag.GroupAnimalID = new SelectList(db.Animals, "AnimalID", "Name", animal.GroupAnimalID);
            ViewBag.BreedID = new SelectList(db.AnimalBreeds, "BreedID", "BreedName", animal.BreedID);
            ViewBag.CreatedBy = new SelectList(db.AnimalUsers, "UserID", "Name", animal.CreatedBy);
            ViewBag.AdoptedByUserID = new SelectList(db.AnimalUsers, "UserID", "Name", animal.AdoptedByUserID);
            return View(animal);
        }

        // POST: Animals/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "AnimalID,Name,AnimalType,BreedID,Sex,Age,Weight,Location,Country,City,Latitude,Longitude,Status,TransportFee,TransportType,GroupAnimalID,Size,HairType,Views,IsDeleted,AdoptedByUserID,CreatedBy,CreatedOn,UpdatedOn")] Animal animal)
        {
            if (ModelState.IsValid)
            {
                db.Entry(animal).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.GroupAnimalID = new SelectList(db.Animals, "AnimalID", "Name", animal.GroupAnimalID);
            ViewBag.BreedID = new SelectList(db.AnimalBreeds, "BreedID", "BreedName", animal.BreedID);
            ViewBag.CreatedBy = new SelectList(db.AnimalUsers, "UserID", "Name", animal.CreatedBy);
            ViewBag.AdoptedByUserID = new SelectList(db.AnimalUsers, "UserID", "Name", animal.AdoptedByUserID);
            return View(animal);
        }

        // GET: Animals/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Animal animal = await db.Animals.FindAsync(id);
            if (animal == null)
            {
                return HttpNotFound();
            }
            return View(animal);
        }

        // POST: Animals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            Animal animal = await db.Animals.FindAsync(id);
            db.Animals.Remove(animal);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
